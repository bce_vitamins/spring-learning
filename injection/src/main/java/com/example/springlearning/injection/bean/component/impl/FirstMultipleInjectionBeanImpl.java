package com.example.springlearning.injection.bean.component.impl;

import com.example.springlearning.injection.bean.component.MultipleInjectionBean;
import com.example.springlearning.injection.bean.postprocessor.Profiling;
import org.springframework.stereotype.Component;

@Component
@Profiling
public class FirstMultipleInjectionBeanImpl implements MultipleInjectionBean {
}
