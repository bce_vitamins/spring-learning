package com.example.springlearning.injection.bean.postprocessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

@Component
public class ProfilingBeanPostProcessor implements BeanPostProcessor {

    private static final String METHOD_NAME = "getSimpleClassName";
    private Map<String, Class> map = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();
        if (beanClass.isAnnotationPresent(Profiling.class)) {
            map.put(beanName, beanClass);
        }
        return null;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class beanClass = map.get(beanName);
        if (beanClass != null) {
            return Proxy.newProxyInstance(beanClass.getClassLoader(), beanClass.getInterfaces(), (proxy, method, args) -> {
                long startTime = System.nanoTime();
                Object result = method.invoke(bean, args);
                if (method.getName().equalsIgnoreCase(METHOD_NAME)) {
                    System.out.println(String.format("%s method %s execution took %s nano seconds",beanName,
                            method.getName(), System.nanoTime() - startTime));
                }
                return result;
            });
        }
        return bean;
    }
}
