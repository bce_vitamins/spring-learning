package com.example.springlearning.injection.bean.service;

import com.example.springlearning.singlebean.bean.BaseBean;

public interface SetterInjectionService extends BaseBean {

    BaseBean getSetterInjectionBean();
}
