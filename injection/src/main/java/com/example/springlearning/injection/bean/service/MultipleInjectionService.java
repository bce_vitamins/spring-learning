package com.example.springlearning.injection.bean.service;

import com.example.springlearning.injection.bean.component.MultipleInjectionBean;
import com.example.springlearning.singlebean.bean.BaseBean;

import java.util.Set;

public interface MultipleInjectionService extends BaseBean {

    Set<MultipleInjectionBean> getAllBeans();
}
