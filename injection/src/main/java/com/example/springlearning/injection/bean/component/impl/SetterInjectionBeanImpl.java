package com.example.springlearning.injection.bean.component.impl;

import com.example.springlearning.injection.bean.component.SetterInjectionBean;
import com.example.springlearning.injection.bean.postprocessor.Profiling;
import org.springframework.stereotype.Component;

@Component
@Profiling
public class SetterInjectionBeanImpl implements SetterInjectionBean {
}
