package com.example.springlearning.injection.bean.service.impl;

import com.example.springlearning.injection.bean.component.FieldInjectionBean;
import com.example.springlearning.injection.bean.service.FieldInjectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FieldInjectionServiceImpl implements FieldInjectionService {

    @Autowired
    private FieldInjectionBean fieldInjectionBean;

    public FieldInjectionBean getFieldInjectionBean() {
        return fieldInjectionBean;
    }
}
