package com.example.springlearning.injection.bean.component.impl;

import com.example.springlearning.injection.bean.postprocessor.Profiling;
import com.example.springlearning.singlebean.bean.BaseBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("SecondQualifiedInjectionBeanImpl")
@Profiling
public class SecondQualifiedInjectionBeanImpl implements BaseBean {
}
