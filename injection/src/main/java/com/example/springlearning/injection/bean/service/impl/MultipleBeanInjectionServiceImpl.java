package com.example.springlearning.injection.bean.service.impl;

import com.example.springlearning.injection.bean.component.MultipleInjectionBean;
import com.example.springlearning.injection.bean.service.MultipleInjectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class MultipleBeanInjectionServiceImpl implements MultipleInjectionService {

    private Set<MultipleInjectionBean> baseBeans;

    @Autowired
    public MultipleBeanInjectionServiceImpl(Set<MultipleInjectionBean> baseBeans) {
        this.baseBeans = baseBeans;
    }

    public Set<MultipleInjectionBean> getAllBeans() {
        return baseBeans;
    }
}
