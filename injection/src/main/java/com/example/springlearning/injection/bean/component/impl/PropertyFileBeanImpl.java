package com.example.springlearning.injection.bean.component.impl;

import com.example.springlearning.singlebean.bean.BaseBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:injection.properties")
public class PropertyFileBeanImpl implements BaseBean {

    @Value("${invalid.class.name}")
    private String invalidClassName;

    @Override
    public String getSimpleClassName() {
        return invalidClassName;
    }
}
