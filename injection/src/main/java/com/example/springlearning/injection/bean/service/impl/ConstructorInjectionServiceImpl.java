package com.example.springlearning.injection.bean.service.impl;

import com.example.springlearning.injection.bean.component.ConstructorInjectionBean;
import com.example.springlearning.injection.bean.postprocessor.Profiling;
import com.example.springlearning.injection.bean.service.ConstructorInjectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConstructorInjectionServiceImpl implements ConstructorInjectionService {

    private ConstructorInjectionBean constructorInjectionBean;

    @Autowired
    public ConstructorInjectionServiceImpl(ConstructorInjectionBean constructorInjectionBean) {
        this.constructorInjectionBean = constructorInjectionBean;
    }

    public ConstructorInjectionBean getConstructorInjectionBean() {
        return constructorInjectionBean;
    }
}
