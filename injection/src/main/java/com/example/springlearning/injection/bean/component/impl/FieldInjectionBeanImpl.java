package com.example.springlearning.injection.bean.component.impl;

import com.example.springlearning.injection.bean.component.FieldInjectionBean;
import com.example.springlearning.injection.bean.postprocessor.Profiling;
import org.springframework.stereotype.Component;

@Component
@Profiling
public class FieldInjectionBeanImpl implements FieldInjectionBean {
}
