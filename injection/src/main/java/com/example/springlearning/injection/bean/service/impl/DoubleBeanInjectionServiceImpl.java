package com.example.springlearning.injection.bean.service.impl;

import com.example.springlearning.injection.bean.service.DoubleBeanInjectionService;
import com.example.springlearning.singlebean.bean.BaseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class DoubleBeanInjectionServiceImpl implements DoubleBeanInjectionService {

    private BaseBean firstQualifiedInjectionBean;

    private BaseBean secondQualifiedInjectionBean;

    @Autowired
    public DoubleBeanInjectionServiceImpl(@Qualifier("FirstQualifiedInjectionBeanImpl") BaseBean firstQualifiedInjectionBean,
                                          @Qualifier("SecondQualifiedInjectionBeanImpl") BaseBean secondQualifiedInjectionBean) {
        this.firstQualifiedInjectionBean = firstQualifiedInjectionBean;
        this.secondQualifiedInjectionBean = secondQualifiedInjectionBean;
    }

    public BaseBean getFirstQualifiedInjectionBean() {
        return firstQualifiedInjectionBean;
    }

    public BaseBean getSecondQualifiedInjectionBean() {
        return secondQualifiedInjectionBean;
    }
}
