package com.example.springlearning.injection.bean.service;

import com.example.springlearning.singlebean.bean.BaseBean;

public interface DoubleBeanInjectionService extends BaseBean {

    BaseBean getFirstQualifiedInjectionBean();

    BaseBean getSecondQualifiedInjectionBean();
}
