package com.example.springlearning.injection.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.example.springlearning.injection.bean")
public class BeanInjectionApplicationConfig {
}
