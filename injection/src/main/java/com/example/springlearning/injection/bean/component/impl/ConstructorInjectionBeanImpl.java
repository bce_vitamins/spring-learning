package com.example.springlearning.injection.bean.component.impl;

import com.example.springlearning.injection.bean.component.ConstructorInjectionBean;
import com.example.springlearning.injection.bean.postprocessor.Profiling;
import org.springframework.stereotype.Component;

@Component
@Profiling
public class ConstructorInjectionBeanImpl implements ConstructorInjectionBean {
}
