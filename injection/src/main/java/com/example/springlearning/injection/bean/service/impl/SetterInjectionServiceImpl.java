package com.example.springlearning.injection.bean.service.impl;

import com.example.springlearning.injection.bean.component.SetterInjectionBean;
import com.example.springlearning.injection.bean.service.SetterInjectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SetterInjectionServiceImpl implements SetterInjectionService {

    private SetterInjectionBean setterInjectionBean;

    @Autowired
    public void setSetterInjectionBean(SetterInjectionBean setterInjectionBean) {
        this.setterInjectionBean = setterInjectionBean;
    }

    public SetterInjectionBean getSetterInjectionBean() {
        return setterInjectionBean;
    }
}
