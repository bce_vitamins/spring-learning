package com.example.springlearning.injection.bean.service;

import com.example.springlearning.singlebean.bean.BaseBean;

public interface FieldInjectionService extends BaseBean {

    BaseBean getFieldInjectionBean();
}
