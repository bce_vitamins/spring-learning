package com.example.springlearning.injection.bean.service;

import com.example.springlearning.singlebean.bean.BaseBean;

public interface ConstructorInjectionService extends BaseBean {

    BaseBean getConstructorInjectionBean();
}
