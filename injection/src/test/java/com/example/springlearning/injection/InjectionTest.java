package com.example.springlearning.injection;

import com.example.springlearning.injection.bean.component.MultipleInjectionBean;
import com.example.springlearning.injection.bean.component.impl.*;
import com.example.springlearning.injection.bean.service.*;
import com.example.springlearning.injection.bean.service.impl.*;
import com.example.springlearning.injection.config.BeanInjectionApplicationConfig;
import com.example.springlearning.singlebean.bean.BaseBean;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InjectionTest {

    private static final String INVALID_CLASS_NAME = "Oops, class name wasn't found";
    private static final ApplicationContext CONTEXT = new AnnotationConfigApplicationContext(BeanInjectionApplicationConfig.class);

    @Test
    void constructorInjectionServiceTest() {
        ConstructorInjectionService constructorInjectionService = CONTEXT.getBean(ConstructorInjectionServiceImpl.class);
        assertEquals(constructorInjectionService.getConstructorInjectionBean().getSimpleClassName(),
                ConstructorInjectionBeanImpl.class.getSimpleName());
    }

    @Test
    void fieldInjectionServiceTest() {
        FieldInjectionService fieldInjectionService = CONTEXT.getBean(FieldInjectionServiceImpl.class);
        assertEquals(fieldInjectionService.getFieldInjectionBean().getSimpleClassName(),
                FieldInjectionBeanImpl.class.getSimpleName());
    }

    @Test
    void setterInjectionServiceTest() {
        SetterInjectionService setterInjectionService = CONTEXT.getBean(SetterInjectionServiceImpl.class);
        assertEquals(setterInjectionService.getSetterInjectionBean().getSimpleClassName(),
                SetterInjectionBeanImpl.class.getSimpleName());
    }

    @Test
    void doubleBeanInjectionServiceTest() {
        DoubleBeanInjectionService doubleBeanInjectionService = CONTEXT.getBean(DoubleBeanInjectionServiceImpl.class);
        assertEquals(doubleBeanInjectionService.getFirstQualifiedInjectionBean().getSimpleClassName(),
                FirstQualifiedInjectionBeanImpl.class.getSimpleName());
        assertEquals(doubleBeanInjectionService.getSecondQualifiedInjectionBean().getSimpleClassName(),
                SecondQualifiedInjectionBeanImpl.class.getSimpleName());
    }

    @Test
    void beansListInjectionServiceTest() {
        MultipleInjectionService multipleInjectionService = CONTEXT.getBean(MultipleBeanInjectionServiceImpl.class);

        Set<String> expectedInjectedBeanClassNames = new HashSet<>();
        expectedInjectedBeanClassNames.add(FirstMultipleInjectionBeanImpl.class.getSimpleName());
        expectedInjectedBeanClassNames.add(SecondMultipleInjectionBeanImpl.class.getSimpleName());

        Set<MultipleInjectionBean> allBeans = multipleInjectionService.getAllBeans();
        assertEquals(allBeans.size(), 2);

        Set<String> actualInjectedBeanClassNames = allBeans.stream()
                .map(BaseBean::getSimpleClassName)
                .collect(Collectors.toSet());
        assertTrue(actualInjectedBeanClassNames.containsAll(expectedInjectedBeanClassNames));
    }

    @Test
    void propertyFileBeanTest() {
        BaseBean propertyFileBean = CONTEXT.getBean(PropertyFileBeanImpl.class);
        assertEquals(propertyFileBean.getSimpleClassName(), INVALID_CLASS_NAME);
    }
}
