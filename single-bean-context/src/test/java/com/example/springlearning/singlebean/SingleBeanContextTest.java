package com.example.springlearning.singlebean;

import com.example.springlearning.singlebean.bean.BaseBean;
import com.example.springlearning.singlebean.bean.impl.AnnotationBean;
import com.example.springlearning.singlebean.bean.impl.JavaConfigBean;
import com.example.springlearning.singlebean.bean.impl.XmlBean;
import com.example.springlearning.singlebean.config.SingleBeanApplicationConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

class SingleBeanContextTest {

    @Test
    void testXmlBean() {
        ApplicationContext context = new ClassPathXmlApplicationContext("config.xml");
        BaseBean xmlBasedBean = context.getBean(XmlBean.class);

        Assertions.assertEquals(xmlBasedBean.getSimpleClassName(), XmlBean.class.getSimpleName());
    }

    @Test
    void testJavaConfigBean() {
        ApplicationContext context = new AnnotationConfigApplicationContext(SingleBeanApplicationConfig.class);
        BaseBean javaConfigBean = context.getBean(JavaConfigBean.class);

        Assertions.assertEquals(javaConfigBean.getSimpleClassName(), JavaConfigBean.class.getSimpleName());
    }

    @Test
    void testAnnotationBean() {
        ApplicationContext context = new AnnotationConfigApplicationContext("com.example.springlearning.singlebean");
        BaseBean annotationBean = context.getBean(AnnotationBean.class);

        Assertions.assertEquals(annotationBean.getSimpleClassName(), AnnotationBean.class.getSimpleName());
    }
}
