package com.example.springlearning.singlebean.bean.impl;

import com.example.springlearning.singlebean.bean.BaseBean;
import org.springframework.stereotype.Component;

@Component
public class AnnotationBean implements BaseBean {
}
