package com.example.springlearning.singlebean.bean;

public interface BaseBean {

    default String getSimpleClassName() {
        return this.getClass().getSimpleName();
    }
}
