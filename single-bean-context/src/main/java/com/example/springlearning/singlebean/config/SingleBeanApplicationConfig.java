package com.example.springlearning.singlebean.config;

import com.example.springlearning.singlebean.bean.BaseBean;
import com.example.springlearning.singlebean.bean.impl.JavaConfigBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SingleBeanApplicationConfig {

    @Bean
    public BaseBean javaConfigBean() {
        return new JavaConfigBean();
    }
}
